Moin Herr Rust,

hier finden Sie mein Projekt für die Berechnung der Prüfziffern eines EAN Codes.

Im Ordner "ean-csharp" finden Sie das Visual Studio Projekt mit der ausführbaren Datei.

Im Ordner "webapp" finden Sie eine Web Version, die in React.js umgesetzt ist. Dort gehen Sie einfach in den /build Ordner und öffnen Sie die index.html Datei. 

Im Ordner "ean-rust" finden Sie eine Version, die in der 
Programmiersprache Rust umgesetzt ist. Die ausführbare Datei befindet 
sich unter /target. Die Datei muss in der Konsole mit dem EAN-Code als 
Aufrufparameter aufgerufen werden.
