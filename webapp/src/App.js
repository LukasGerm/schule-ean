import React, {Component} from 'react';
import {Grid, Typography, TextField} from "@material-ui/core";
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ean: '',
            error: false,
            errorMsg: ''
        };
        this.checkEan = this.checkEan.bind(this);
    }

    checkEan(event) {
        let ean = event.target.value;
        if (ean.toLocaleString().length !== 8 && ean.toLocaleString().length !== 13) {
            this.setState({
                ean: ean,
                error: true,
                errorMsg: 'Your EAN Code has to be 8 or 13 digits',
                successMsg: ""
            })
        } else {
            let summary = 0;
            let eanList = ean.split("");
            eanList.pop();
            for (let i= eanList.length -1 ; i>= 0; i--)
                summary += (i-(eanList.length - 1) * -1) %2 === 0 ? parseInt(eanList[i]) * 3 : parseInt(eanList[i]);

            
            let hashsum = 0;
            while (summary % 10 !== 0) {
                hashsum++;
                summary++;
            }
            eanList.push(hashsum.toLocaleString());
            if (ean === eanList.join(""))
                this.setState({
                    ean: ean,
                    error: false,
                    errorMsg: "",
                    successMsg: "Your ean code is correct"
                });
            else
                this.setState({
                    ean: ean,
                    error: true,
                    errorMsg: "Your checksum is false. Your EAN Code has to be " + eanList.join(""),
                    successMsg: ""
                })
        }

    }

    render() {

        return (
            <Grid container alignContent="center" justify="center" direction="row">
                <Grid item xs={8} style={{marginTop: "10%"}}>
                    <Typography variant="h2" style={{textAlign: "center"}}>Hello! Please put in your EAN
                        code!</Typography>
                </Grid>
                <Grid item xs={8} style={{marginTop: '2%'}}>
                    <Typography variant="body1"
                                style={{textAlign: "center", color: "red"}}>{this.state.errorMsg}</Typography>
                    <Typography variant="body1"
                                style={{textAlign: "center", color: "green"}}>{this.state.successMsg}</Typography>
                </Grid>
                <Grid container alignContent="center" justify="center" direction="row">
                    <TextField
                        id="outlined-name"
                        label="EAN"
                        value={this.state.ean}
                        onChange={this.checkEan}
                        margin="normal"
                        error={this.state.error}
                        variant="outlined"
                        style={{marginTop: "2%"}}
                    />
                </Grid>

            </Grid>
        );
    }
}

export default App;
