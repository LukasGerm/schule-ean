use std::iter::FromIterator;

fn main() {
    let ean= std::env::args().nth(1).expect("No ean given");
    if ean.chars().count() != 8 && ean.chars().count() != 13 {
        println!("Ean has to be 8 or 13 digits long");

    }
    else {
        let mut _ean_vec: Vec<u32> = Vec::new();
        let _ean_string_vec: Vec<char> = ean.chars().collect();
        //creates array
        for c in _ean_string_vec {
            _ean_vec.push(c.to_digit(10).unwrap())
        }
        //remove the last digit
        _ean_vec.pop();
        let mut _summary = 0;
        let mut _hashsum = 0;
        //loop reversly through the vector

        let mut i = (_ean_vec.len() as i32) - 1;

        loop {
            
            if (i - (_ean_vec.len() as i32 - 1) * -1) % 2 == 0 {
                _summary += _ean_vec[i as usize]* 3;
            }
            else {
                _summary += _ean_vec[i as usize];
            }
            if i == 0{
                break;
            }
            i = i-1;
        }
        while _summary % 10 != 0 {
            _hashsum = _hashsum + 1;
            _summary = _summary + 1;
        }
        let _orig_hash_sum_char = ean.chars().last().unwrap();
        if _orig_hash_sum_char.to_digit(10).unwrap() == _hashsum {
            println!("Your ean was correct");
        }
        else {
            println!("Your ean was not correct. The correct one would be {}{}", _ean_vec.into_iter().map(|i| i.to_string()).collect::<String>() ,_hashsum);
            println!("Hashsum: {}", _hashsum);
        }
        
    }
}
