﻿using System;
using System.Runtime.CompilerServices;

namespace ean_console
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Press enter to begin");
            while (Console.ReadLine() != "x")
            {
                Console.WriteLine("Please type in your EAN code and press enter (7,8, 12,13)");
                long ean;
                //While the inputted ean doesnt has the type int or long, check the 
                while (true)
                {
                    //check if the ean typed in is type long (int)
                    if (!long.TryParse(Console.ReadLine(), out ean))
                        Console.WriteLine("Please put in a number ");
                    else
                    {
                        //check if the ean has the length of 8 or 13 
                        if (ean.ToString().Length != 8 && ean.ToString().Length != 13 && ean.ToString().Length != 7 && ean.ToString().Length != 12)
                            Console.WriteLine("The ean does not have the right length");
                        else
                            break;
                    }
                }

                //create a new ean object
                EAN eanObject = new EAN(ean);
                //if the ean put in, is the same as the corrected one, put that out
                if (eanObject.Validate())
                    Console.WriteLine("The ean you put in is correct");
                else
                {
                    //output the right ean with hashsum and then the hashsum
                    Console.WriteLine("The correct ean is: " + eanObject.GetEan() +
                                      "\nThe correct hashsum is: " + eanObject.GetHashSum());
                }

                Console.WriteLine("Please press x if you want to exit, press enter if you want to continue");
            }
        }
    }
}