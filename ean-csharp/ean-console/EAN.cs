using System;
using System.Linq;
using System.Collections.Generic;
namespace ean_console
{
    public class EAN
    {
        /// <summary>
        /// The ean given to the constructor
        /// </summary>
        private long OrigEAN;
        /// <summary>
        /// the ean number without the hashsum
        /// </summary>
        private List<long> _eanNumber;



        ///<summary>
        ///calculated Hashsum saved
        ///</summary>
        private int _hashSum;

        public EAN(long ean)
        {
            OrigEAN = ean;
            //The summary
            long summary = 0;
            List<long> eanList = new List<long>();
            do {
                eanList.Insert(0, ean % 10);
                ean /= 10;
            } while (ean != 0);

            //remove the hashsum
            if(!(eanList.Count != 8 && eanList.Count != 13))
                eanList.RemoveAt(eanList.Count - 1);
            _eanNumber = eanList;
            for (int i = eanList.Count - 1; i >= 0; i--)
                summary += (uint)(i - (eanList.Count - 1)) % 2 == 0 ? eanList[i] * 3 : eanList[i];
            int hashsum = 0;
            //count to the next modulo 10 == 0
            while (summary % 10 != 0)
            {
                hashsum++;
                summary++;

            }

            //add the new hashsum
            _hashSum = hashsum;

        }
        /// <summary>
        /// Method to check the ean code
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            //if the calculated ean == the original ean, put out true, else false
            return long.Parse(string.Join("", _eanNumber) + _hashSum) == OrigEAN ? true : false;
        }
        /// <summary>
        /// getter for the ean number
        /// </summary>
        /// <returns>the ean number</returns>
        public long GetEan()
        {
            return long.Parse(string.Join("", _eanNumber)+_hashSum);
        }
        /// <summary>
        /// Method to get the hashsum
        /// </summary>
        /// <returns>hashsum</returns>
        public long GetHashSum()
        {
            return _hashSum;
        }
    }
}